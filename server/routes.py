import timeit
import time
from flask import Blueprint, render_template, session, redirect, url_for, request, g, jsonify
from .auth import multi_optional_auth, multi_auth, get_token
from .models import Instance, db
import datetime

main = Blueprint('main', __name__)


@main.route('/set', methods=['GET'])
@multi_optional_auth.login_required
def set_clock():
    if g.current_user is not None:
        return render_template('set.html.j2')
    else:
        return redirect(url_for('main.login'))


@main.route('/', methods=['GET'])
@main.route('/clock', methods=['GET'])
@multi_optional_auth.login_required
def clock():
    return render_template('clock.html.j2')


@main.route('/clockWithCountdown', methods=['GET'])
@multi_optional_auth.login_required
def clock_with_countdown():
    return render_template('clockWithCountdown.html.j2')


@main.route('/systemtime', methods=['POST'])
def system_time():
    start = timeit.timeit()
    response = {}
    instance = Instance.query.filter_by(instance_name="server").one_or_none()
    response['serverTimestamp'] = int(time.time_ns() / (10 ** 6)) + instance.time_offset - 150
    response['clientTimestamp'] = request.json["clientTimestamp"]
    response['delta'] = max(0, round(timeit.timeit() - start, 2))
    return jsonify(response)


@main.route('/settime', methods=['POST'])
@multi_auth.login_required
def set_time_post():
    print("Setting Time: " + str(request.json))
    json = request.json
    set_time = datetime.datetime.now().replace(hour=int(json['hours']), minute=int(json['minutes']),
                                               second=int(json['seconds']))
    instance = Instance.query.filter_by(instance_name="server").one_or_none()
    instance.time_offset = int((set_time - datetime.datetime.now()).total_seconds() * 1000)
    db.session.commit()
    return ''


@main.route('/login', methods=['GET'])
def login():
    g.current_user = None
    if g.current_user is not None:
        return redirect(url_for('main.set_clock'))
    return render_template('login.html.j2')


@main.route('/login', methods=['POST'])
@multi_auth.login_required
def login_user():
    if g.current_user is None:
        redirect(url_for('main.login'))
    else:
        token = get_token()
        resp = redirect(url_for('main.set_clock'))
        resp.set_cookie('Bearer', token)
        return resp


@main.route("/logout")
def logout():
    session['logged_in'] = False
    resp = redirect(url_for('main.login'))
    resp.set_cookie('Bearer', '', expires=(datetime.datetime.now() - datetime.timedelta(days=5)))
    return resp
