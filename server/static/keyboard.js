let Keyboard = window.SimpleKeyboard.default;

var keyboard = null;;

function onKeyPress(button) {
  console.log("Button pressed", button);

  var active = document.activeElement;

  if (button === "{bksp}") {
    active.value = active.value.substring(0, active.value.length - 1);
  } else if (button === "close") {
    keyboard.destroy();
    keyboard = null;
  } else {
    active.value = active.value + button;
  }

  console.log("Finished")
}

function getKeyboardButtonText() {
  if (keyboard === null) {
    return "Open Keypad";
  } else {
    return "Close Keypad";
  }
}

function openKeyboard() {
  if (keyboard === null) {
    keyboard = new Keyboard({
      onKeyPress: button => onKeyPress(button),
      preventMouseDownDefault: true,
      layout: {
        default: ["1 2 3", "4 5 6", "7 8 9", "0 {bksp}"],
      },
      theme: "hg-theme-default hg-layout-numeric numeric-theme"
    });
  } else {
    keyboard.destroy();
    keyboard = null;
  }

  document.getElementById("openKeyboard").innerText = (keyboard === null ? "Open Keypad" : "Close Keypad");

}