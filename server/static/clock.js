const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const syncedHTML = "<span style='color: green;'>Synced</span>";
const syncingHTML = "<span style='color: darkred;'>Syncing</span>";

// All in milliseconds
const SCREEN_REFRESH_INTERVAL = 15;
const DATE_SYNC = 60000;
const SET_SYNC_INFO = 1000;
const TIMEOUT_DELAY = 10000;
const QUICK_SYNC = 200;
const REGULAR_SYNC = 4000;
const MAX_CHANGE_DURING_SYNC = 200;
const LIMIT_TO_CALL_SYNCED = 30;

let offset = 2000;
let timeDelta = 0;
let screenBlankedTime = new Date();

let runningBurnInPrevention = false;

let t = null;
let d = null;
let s = null;

function startClock() {
    roughTime();

    // Start timer to refresh date/time from server
    t = setInterval(setTime, SCREEN_REFRESH_INTERVAL);
    d = setInterval(setDate, DATE_SYNC);
    s = setInterval(setSync, SET_SYNC_INFO);

    setSync();
    setDate();
    syncTime();
}

function syncTime() {
    if (runningBurnInPrevention) {
        return
    }

    fetchWithTimeout('/systemtime', {
        method: 'post',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({clientTimestamp: (timeDelta + window.performance.now())})
    }, TIMEOUT_DELAY).then(function (response) {
        if (response != null) {
            return response.json()
        } else {
            return null;
        }
    }).then(function (json) {
        if (json == null) {
            throw "Error, json is null";
        }

        let recTimestamp = (timeDelta + window.performance.now());
        let roundTrip = Math.round(recTimestamp - json.clientTimestamp);
        offset = recTimestamp - (json.serverTimestamp + roundTrip / 2);
        //console.log(offset + " " + roundTrip + " " + recTimestamp + " " + json.serverTimestamp + " " + json.delta + " " + json.clientTimestamp);
        if (offset > 10000 || offset < -10000) {
            roughTime();
            setTimeout(syncTime, QUICK_SYNC);
        } else {
            let change = 0;
            if (offset > 10) {
                change = Math.min(offset / 5, MAX_CHANGE_DURING_SYNC);
            } else if (offset < -10) {
                change = Math.max(offset / 5, -MAX_CHANGE_DURING_SYNC);
            }
            if (change > 2 || change < -2) {
                timeDelta = timeDelta - change;
                setTimeout(syncTime, QUICK_SYNC);
            } else {
                setTimeout(syncTime, REGULAR_SYNC);
                resetErrored();
            }
        }
    }).catch(function (err) {
        setErrored(err);
        setTimeout(syncTime, 4000);
    });
}

function setSync() {
    if (isNaN(offset) || offset > LIMIT_TO_CALL_SYNCED || offset < -LIMIT_TO_CALL_SYNCED) {
        document.getElementById("syncing").innerHTML = syncingHTML + " with offset of: " + Math.round(offset) + "ms";
    } else {
        document.getElementById("syncing").innerHTML = syncedHTML + " with offset of: " + Math.round(offset) + "ms";
    }
}

function setDate() {
    let today = new Date(Math.round(window.performance.now() + timeDelta));

    let curWeekDay = days[today.getDay()];
    let curDay = today.getDate();
    let curMonth = months[today.getMonth()];
    let curYear = today.getFullYear();
    document.getElementById("date").innerHTML = curWeekDay + ", " + curDay + " " + curMonth + " " + curYear;
}

function padTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function roughTime() {
    fetchWithTimeout('/systemtime', {
        method: 'post',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({clientTimestamp: (timeDelta + window.performance.now())})
    }, TIMEOUT_DELAY).then(function (response) {
        if (response != null) {
            return response.json();
        } else {
            return null;
        }
    }).then(function (json) {
        if (json == null) {
            throw "Error, json is null";
        }
        let currentTime = new Date(json.serverTimestamp);
        timeDelta = currentTime - Math.round(window.performance.now());
        setDate();
    }).catch(setErrored);
}

function setServerTime() {
    let payload = {
        hours: document.getElementById('hours').value,
        minutes: document.getElementById('minutes').value,
        seconds: document.getElementById('seconds').value
    };

    fetchWithTimeout('/settime', {
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(payload)
    }, TIMEOUT_DELAY).then(roughTime).catch(setErrored);
}

function setInputs() {
    fetchWithTimeout('/systemtime', {
        method: 'post',
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({clientTimestamp: (timeDelta + window.performance.now())})
        }, TIMEOUT_DELAY).then(function (response) {
            if (response != null) {
                return response.json()
            } else {
                return null;
            }
        }).then(function (json) {
            if (json == null) {
                throw "Error, json is null";
            }
            let x = new Date(json.serverTimestamp);
            document.getElementById('hours').value = x.getHours();
            document.getElementById('minutes').value = x.getMinutes();
            document.getElementById('seconds').value = x.getSeconds();
    }).catch(setErrored);
}

function fetchWithTimeout(url, options, delay) {
   const timer = new Promise((resolve) => {
      setTimeout(resolve, delay, {
        timeout: true,
      });
   });
   return Promise.race([
      fetch(url, options),
      timer
   ]).then(function(response) {
       if (response.timeout) {
           throw "Error, timeout from server"
       }
       return response;
   });
}

function setErrored(err) {
    console.log(err);
    document.body.style.background = "#DC143C";
    document.getElementById("error-div").innerText = "Error connecting to time server";
    offset = NaN;
}

function resetErrored() {
    document.getElementById("error-div").innerText = "";
    document.body.style.background = "#3498DB";
}

function blankScreen() {
    // Check if blanking is needed
    let now = new Date(Math.round(window.performance.now() + timeDelta));
    let hr = now.getHours();
    now.setHours(now.getHours() - 12);
    if (hr === 0 && screenBlankedTime < now) {
        // Run blanking
        clearInterval(t);
        clearInterval(d);
        clearInterval(s);

        runningBurnInPrevention = true;

        let body = document.getElementsByTagName("BODY")[0];
        body.innerHTML = "<h2 style='text-align: center'>Preventing Burn In</h2>";

        cycleBackgroundColors(0);
    }
}

function cycleBackgroundColors(color) {
    let colors = ["#000000", "#FFFFFF", "#FFFF00", "#FF00FF", "#00FFFF"];
    if (color < colors.length) {
        document.body.style.background = colors[color];

        setTimeout(function () {
            cycleBackgroundColors(color + 1);
        }, 3000);
    } else {
        setTimeout(() => window.location.reload(false), 3000);
    }
}