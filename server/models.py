from datetime import datetime
import bcrypt
import string
from . import db

chars = string.ascii_letters + '0123456789'
epoch = datetime.utcfromtimestamp(0)


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False,
                                   default=datetime.utcnow)


class User(BaseModel):
    __tablename__ = 'users'
    username = db.Column(db.String(40), nullable=False)
    password = db.Column(db.LargeBinary(100), nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)

    def verify_password(self, password):
        return bcrypt.checkpw(password.encode('utf-8'), self.password)

    def set_password(self, new_password, old_password=""):
        if self.password is None or self.password == "":
            self.password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
            return True
        elif bcrypt.checkpw(old_password.encode('utf-8'), self.password):
            self.password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
            return False

    def __init__(self, username="", password="", email="", display_name=""):
        self.username = username
        if not password == "":
            self.set_password(password)
        self.email = email
        self.display_name = display_name

    def __str__(self):
        return 'Username: ' + self.username + ' id: ' + str(self.id)

    def __repr__(self):
        return 'User()'

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.id == other.id
        return NotImplemented

    def __ne__(self, other):
        """Define a non-equality test"""
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented


class AllowedIP(BaseModel):
    __tablename__ = 'allowed'
    ip_addr = db.Column(db.String(16), nullable=False)
    last_query = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)


class Instance(BaseModel):
    __tablename__ = 'instance'
    time_offset = db.Column(db.Integer, nullable=False, default=0)
    instance_name = db.Column(db.String(40), nullable=False, unique=True)
    config_vals = db.relationship("InstanceConfigValue", back_populates="instance")

    def __init__(self, name):
        self.instance_name = name
        self.time_offset = 0


class InstanceConfigValue(BaseModel):
    __tablename__ = 'config'
    instance_id = db.Column(db.Integer, db.ForeignKey("instance.id"))
    instance = db.relationship("Instance", back_populates="config_vals")
    value_name = db.Column(db.String(40), nullable=False)
    value = db.Column(db.String(40), nullable=True)
