import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import OperationalError

from config import config

db = SQLAlchemy()

from . import models


def create_app(config_name=None):
    # This line will allow for overriding configs but defaults to production
    if config_name is None:
        config_name = os.environ.get('NTP_CONFIG', 'production')
    config_name = config_name.strip()
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.host = '0.0.0.0'
    # Initialize flask extensions
    db.init_app(app)

    # Register web application routes
    from .routes import main as main_blueprint
    from .auth import auth_blueprint
    app.register_blueprint(main_blueprint)
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    try:
        with app.app_context():
            instance = models.Instance.query.filter_by(instance_name="server").one_or_none()
            if not instance:
                instance = models.Instance("server")
                db.session.add(instance)
                db.session.commit()
    except OperationalError:
        print("Failed to create instance for server")

    return app
