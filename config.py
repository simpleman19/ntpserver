import os
import binascii

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///../ntpserver.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    SECRET_KEY = '1f52814'


class ProductionConfig(Config):
    secret = None
    if os.path.isfile('secret.txt'):
        with open('secret.txt', 'r') as secret_in:
            secret = secret_in.read()
    if secret is None:
        secret = binascii.hexlify(os.urandom(24))
    SECRET_KEY = secret


class TestingConfig(Config):
    TESTING = True


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
