import os
import random
import subprocess
import sys
import binascii

from flask_script import Manager, Command
from livereload import Server

from config import DevelopmentConfig
from server import create_app, db
from server.models import User

manager = Manager(create_app)

root_path = os.path.dirname(os.path.abspath(__file__))


def _make_context():
    return dict(app=manager.app, db=db)


class LiveReloadServer(Command):
    def run(self):
        app = manager.app
        app.debug = True
        server = Server(app.wsgi_app)
        server.serve()


@manager.option('-d', '--drop_first', help='Drop tables first?')
def createdb(drop_first=True):
    """Creates the database."""
    db.session.commit()
    db.drop_all()
    db.create_all()


@manager.command
def test():
    """Run unit tests"""
    tests = subprocess.call(['python', '-m', 'pytest'])
    sys.exit(tests)


@manager.command
def seed():
    db.session.commit()
    db.drop_all()
    db.create_all()


@manager.command
def create_user():
    user = User()
    user.username = input('Username: ')
    user.set_password(input('Password: '))
    user.is_admin = input('Is Admin?: ') == "True"
    db.session.add(user)
    db.session.commit()


@manager.command
def hash_pass():
    user = User()
    user.set_password(input('Password: '))
    print('Hashed: ', binascii.hexlify(user.password))


@manager.command
def load_users():
    with open('hashed.txt', 'r') as hashed_pass:
        for f in hashed_pass.readlines():
            user = User()
            user.username = f.split(':')[0]
            user.password = binascii.unhexlify(f.split(':')[1])
            user.is_admin = f.split(':')[2] == "True"
            db.session.add(user)
    db.session.commit()


manager.add_command("livereload", LiveReloadServer)

if __name__ == '__main__':
    manager.run()
