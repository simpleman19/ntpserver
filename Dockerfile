FROM python:3.7.0-alpine3.8

RUN apk update && apk add git make gcc libc-dev libffi-dev openssl-dev tzdata coreutils g++

# Clean APK cache
RUN rm -rf /var/cache/apk/*

ENV TZ=America/Chicago

RUN mkdir /deploy

WORKDIR /deploy

COPY requirements.txt /deploy/requirements.txt
RUN pip3 install -r requirements.txt

COPY . /deploy/

ENV NTP_CONFIG=production

RUN python3 manage.py createdb
RUN python3 manage.py load_users && rm hashed.txt
RUN echo $NTP_CONFIG

EXPOSE 5000

ENTRYPOINT sh entrypoint.sh