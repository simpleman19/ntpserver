import datetime
import pytz

local = pytz.timezone("America/Chicago")

curr = pytz.utc.localize(datetime.datetime.now())
'''expected = datetime.datetime.strptime('{date} {hours:02d}:{minutes:02d}:{seconds:02d}'
                                      .format(date=datetime.datetime.today().strftime('%Y-%m-%d'),
                                              hours=int(json['hours']), minutes=int(json['minutes']),
                                              seconds=int(json['seconds'])), '%Y-%m-%d %H:%M:%S')'''
expected = datetime.datetime.now()
expected = expected.replace(hour=20, minute=14, second=0)
utc_expected = local.localize(expected).astimezone(pytz.utc)

print((curr - utc_expected).total_seconds())