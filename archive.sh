#!/bin/bash

rm ntpServer.tgz

mkdir /tmp/ntp

cp -r * /tmp/ntp

tar --exclude=hashed.txt --exclude=.git --exclude=*.pyc --exclude=__pycache__ --exclude=.idea --exclude=ntpServer.tgz -zcvf ntpServer.tgz -C /tmp/ntp/ .

rm -rf /tmp/ntp
