This is a project written in flask that allows for adjusting a central clock through a web application.  This clock can
either be viewed using a web browser or can be synced to using ntp.  This project has two pieces, the web server and
the ntp server.  Both of these reference an offset that is stored in the database to be able to return what time should
be showed to the user and synced to.  It was designed to run on a raspberry pi with a screen to show the clock.

The architecture is very basic, it simply uses a web client that can be used to view or set the clock.  When the clock
is set, the applicaiton calculates an offset of the requested time vs the actual system time.  This offset is stored
in the database and referenced whenever a system is requesting the time over the rest api or over ntp.

Potential improvements could be:
- allow for multiple different clock instances on the same server.  This would allow one 
server to handle multiple clocks.
   
- move database out of sqlite to a real database for speed improvement

- improve syncing algo, specifically on firefox.  Firefox seems to occasionally have issues keeping the clock synced

First thing to do is to set up a virtual env
`python -m venv .env`

Then activate it
`. .env/bin/activate`

To install dependencies run
`pip install -r requirements.txt`

To create the db `python manage.py createdb`

To create a user `python manage.py create_user`

To run the web app `python manage.py runserver`

To build the docker image `./build.sh`

To run the image `./run.sh`

To stop the image `./stop.sh`

In order for a build to work, the hashed password file needs to be unencrypted this requires
running `gpg -o hashed.txt hashed.txt.gpg` or if you don't want to build with any users then just create an empty
hashed.txt

hashed.txt is built using the following format: `<username>:<brcryped_pass>:<is_admin>`
The bcrypted pass can be created using `python manage.py hash_pass`

The ntp server can be started using `python ntp/ntp_server.py`  This reads the same offset as is used on the website
so that an ntp client can be synced to the server