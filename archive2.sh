#!/bin/bash

rm ntpServer.tgz

mkdir /tmp/ntp

cp -r . /tmp/ntp

tar --exclude=hashed.txt -zcvf ntpServer2.tgz -C /tmp/ntp/ .

rm -rf /tmp/ntp
