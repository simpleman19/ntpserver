import datetime
import socket
import struct
import time
import threading
import select
import sqlalchemy
from queue import Queue, Empty

task_queue = Queue()
stop_flag = False

instance_query = 'select instance.time_offset from instance where instance.instance_name = "server"'
server_uri = 'sqlite:///ntpserver.db'


def system_to_ntp_time(timestamp):
    """Convert a system time to a NTP time.

    Parameters:
    timestamp -- timestamp in system time

    Returns:
    corresponding NTP time
    """
    return timestamp + NTP.NTP_DELTA


def _to_frac(timestamp, n=32):
    """Return the fractional part of a timestamp.

    Parameters:
    timestamp -- NTP timestamp
    n         -- number of bits of the fractional part

    Retuns:
    fractional part
    """
    return int(abs(timestamp - int(timestamp)) * 2 ** n)


def _to_time(integ, frac, n=32):
    """Return a timestamp from an integral and fractional part.

    Parameters:
    integ -- integral part
    frac  -- fractional part
    n     -- number of bits of the fractional part

    Retuns:
    timestamp
    """
    return integ + float(frac) / 2 ** n


class NTPException(Exception):
    """Exception raised by this module."""
    pass


class NTP:
    """Helper class defining constants."""

    _SYSTEM_EPOCH = datetime.date(*time.gmtime(0)[0:3])
    _NTP_EPOCH = datetime.date(1900, 1, 1)
    NTP_DELTA = (_SYSTEM_EPOCH - _NTP_EPOCH).days * 24 * 3600
    """delta between system and NTP time"""


class NTPPacket:
    """NTP packet class.

    This represents an NTP packet.
    """

    _PACKET_FORMAT = "!B B B b 11I"
    """packet format to pack/unpack"""

    def __init__(self, version=2, mode=3, tx_timestamp=0):
        """Constructor.

        Parameters:
        version      -- NTP version
        mode         -- packet mode (client, server)
        tx_timestamp -- packet transmit timestamp
        """
        self.leap = 0
        """leap second indicator"""
        self.version = version
        """version"""
        self.mode = mode
        """mode"""
        self.stratum = 0
        """stratum"""
        self.poll = 0
        """poll interval"""
        self.precision = 0
        """precision"""
        self.root_delay = 0
        """root delay"""
        self.root_dispersion = 0
        """root dispersion"""
        self.ref_id = 0
        """reference clock identifier"""
        self.ref_timestamp = 0
        """reference timestamp"""
        self.orig_timestamp = 0
        self.orig_timestamp_high = 0
        self.orig_timestamp_low = 0
        """originate timestamp"""
        self.recv_timestamp = 0
        """receive timestamp"""
        self.tx_timestamp = tx_timestamp
        self.tx_timestamp_high = 0
        self.tx_timestamp_low = 0
        """transmit timestamp"""

    def to_data(self):
        """Convert this NTPPacket to a buffer that can be sent over a socket.

        Returns:
        buffer representing this packet

        Raises:
        NTPException -- in case of invalid field
        """
        try:
            packed = struct.pack(NTPPacket._PACKET_FORMAT,
                                 (self.leap << 6 | self.version << 3 | self.mode),
                                 self.stratum,
                                 self.poll,
                                 self.precision,
                                 int(self.root_delay) << 16 | _to_frac(self.root_delay, 16),
                                 int(self.root_dispersion) << 16 |
                                 _to_frac(self.root_dispersion, 16),
                                 self.ref_id,
                                 int(self.ref_timestamp),
                                 _to_frac(self.ref_timestamp),
                                 self.orig_timestamp_high,
                                 self.orig_timestamp_low,
                                 int(self.recv_timestamp),
                                 _to_frac(self.recv_timestamp),
                                 int(self.tx_timestamp),
                                 _to_frac(self.tx_timestamp))
        except struct.error:
            raise NTPException("Invalid NTP packet fields.")
        return packed

    def from_data(self, data):
        """Populate this instance from a NTP packet payload received from
        the network.

        Parameters:
        data -- buffer payload

        Raises:
        NTPException -- in case of invalid packet format
        """
        try:
            unpacked = struct.unpack(NTPPacket._PACKET_FORMAT,
                                     data[0:struct.calcsize(NTPPacket._PACKET_FORMAT)])
        except struct.error:
            raise NTPException("Invalid NTP packet.")

        self.leap = unpacked[0] >> 6 & 0x3
        self.version = unpacked[0] >> 3 & 0x7
        self.mode = unpacked[0] & 0x7
        self.stratum = unpacked[1]
        self.poll = unpacked[2]
        self.precision = unpacked[3]
        self.root_delay = float(unpacked[4]) / 2 ** 16
        self.root_dispersion = float(unpacked[5]) / 2 ** 16
        self.ref_id = unpacked[6]
        self.ref_timestamp = _to_time(unpacked[7], unpacked[8])
        self.orig_timestamp = _to_time(unpacked[9], unpacked[10])
        self.orig_timestamp_high = unpacked[9]
        self.orig_timestamp_low = unpacked[10]
        self.recv_timestamp = _to_time(unpacked[11], unpacked[12])
        self.tx_timestamp = _to_time(unpacked[13], unpacked[14])
        self.tx_timestamp_high = unpacked[13]
        self.tx_timestamp_low = unpacked[14]

    def get_tx_timestamp(self):
        return self.tx_timestamp_high, self.tx_timestamp_low

    def set_origin_timestamp(self, high, low):
        self.orig_timestamp_high = high
        self.orig_timestamp_low = low


class RecvThread(threading.Thread):
    def __init__(self, socket_con):
        threading.Thread.__init__(self)
        self.socket = socket_con

    def run(self):
        global task_queue, stop_flag
        while True:
            if stop_flag:
                print("RecvThread Ended")
                break
            rlist, wlist, elist = select.select([self.socket], [], [], 1)
            if len(rlist) != 0:
                print("Received %d packets" % len(rlist))
                for temp_socket in rlist:
                    try:
                        data, addr = temp_socket.recvfrom(1024)
                        recv_timestamp = system_to_ntp_time(time.time())
                        task_queue.put((data, addr, recv_timestamp))
                    except Exception as ex:
                        print(ex)


class WorkThread(threading.Thread):
    def __init__(self, socket_con):
        threading.Thread.__init__(self)
        self.socket = socket_con

    def run(self):
        global task_queue, stop_flag
        engine = sqlalchemy.create_engine(server_uri, echo=False)
        while True:
            if stop_flag:
                print("WorkThread Ended")
                break
            try:
                data, addr, recv_timestamp = task_queue.get(timeout=1)
                db_conn = engine.connect()
                res = db_conn.execute(instance_query)
                row = res.fetchone()
                if row:
                    timeoffset = row[0] / 1000
                else:
                    timeoffset = 0

                recv_packet = NTPPacket()
                recv_packet.from_data(data)
                timestamp_high, timestamp_low = recv_packet.get_tx_timestamp()
                send_packet = NTPPacket(version=3, mode=4)
                send_packet.stratum = 1
                send_packet.poll = 10
                # send_packet.precision = 0xfa
                # send_packet.root_delay = 0x0bfa
                # send_packet.root_dispersion = 0x0aa7
                # send_packet.ref_id = 0x808a8c2c
                send_packet.ref_timestamp = recv_timestamp - 5 + timeoffset
                send_packet.set_origin_timestamp(timestamp_high, timestamp_low)
                send_packet.recv_timestamp = recv_timestamp + timeoffset
                send_packet.tx_timestamp = system_to_ntp_time(time.time()) + timeoffset
                self.socket.sendto(send_packet.to_data(), addr)
                db_conn.close()
                print("Sent to %s:%d with offset: %d" % (addr[0], addr[1], timeoffset))
            except Empty:
                pass


def bootstrap_server(ip: str, port: int):
    global stop_flag
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((ip, port))
    print("local socket: ", sock.getsockname())
    rev_thread = RecvThread(sock)
    rev_thread.start()
    work_thread = WorkThread(sock)
    work_thread.start()

    print("Booted server...")
    while True:
        try:
            time.sleep(0.5)
        except KeyboardInterrupt:
            print("Exiting...")
            stop_flag = True
            rev_thread.join()
            work_thread.join()
            print("Exited")
            break


if __name__ == '__main__':
    bootstrap_server('0.0.0.0', 123)
