#!/bin/sh

export NTP_CONFIG=development
export PYTHONUNBUFFERED=1

python ntp/ntp_server.py &

gunicorn --config /deploy/gunicorn_config.py --keep-alive 5 --timeout 30 wsgi:app
