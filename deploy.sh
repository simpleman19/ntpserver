#!/bin/bash

PS3='Where should I deploy?: '
options=("edmclock.therealcturner.com" "ntpserver.unity.com" "10.0.0.84" "10.0.0.86" "10.0.0.87" "10.0.0.98")
select hostname in "${options[@]}"
do
    ./archive.sh
    ssh simpleman19@${hostname} "mkdir /home/simpleman19/ntpServer"
    scp ntpServer.tgz simpleman19@${hostname}:/home/simpleman19/ntpServer
    ssh -t simpleman19@${hostname} "unset DISPLAY && cd /home/simpleman19/ntpServer && tar -xvf ntpServer.tgz && cp run.sh ../ &&  gpg --output hashed.txt -d hashed.txt.gpg && ./build.sh && ./stop.sh || true && ./run.sh && cd ~/ && rm -rf /home/simpleman19/ntpServer"
    exit
done